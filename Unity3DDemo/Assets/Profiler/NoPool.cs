﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConstantVar
{
    public static int totalCount = 50;

    public static int currCount = 10;
}

public class NoPool : MonoBehaviour
{
    private void Prawn()
    {
        if (totalCount > ConstantVar.totalCount)
        {
            this.CancelInvoke();
            return;
        }

        totalCount++;

        Object oo = Resources.Load("go_prefab");
        GameObject ui = (GameObject)GameObject.Instantiate(oo);

        list.Add(ui);
    }

    private List<GameObject> list = new List<GameObject>();
    private int totalCount = 0;

    public void OnGUI()
    {
        if (GUILayout.Button("Pawn", GUILayout.Height(30)))
        {
            this.InvokeRepeating("Prawn", 1f, 1f);
        }

        GUILayout.TextArea("Curr:" + list.Count.ToString() + ", total: "+totalCount);
    }

    public void Update()
    {
        if(list.Count > ConstantVar.currCount)
        {
            GameObject.DestroyImmediate(list[0]);
            list.RemoveAt(0);
        }
    }


}
