﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PathologicalGames;

public class Pool : MonoBehaviour
{
    public void Start()
    {
        Application.targetFrameRate = 10;
    }

    private void Prawn()
    {
        if (totalCount > ConstantVar.totalCount)
        {
            this.CancelInvoke();
            return;
        }

        totalCount++;

        Transform trans = PoolManager.Pools[PoolName].Spawn(prefab);
        list.Add(trans.gameObject);
    }

    private List<GameObject> list = new List<GameObject>();
    private int totalCount = 0;
    private string PoolName = "pool1";
    private GameObject prefab;
    public void OnGUI()
    {
        if (GUILayout.Button("Pool Pawn", GUILayout.Height(30)))
        {
            if (prefab == null)
            {
                SpawnPool p = PoolManager.Pools.Create(this.PoolName);
                p.poolName = PoolName;               
                prefab = Resources.Load("go_prefab") as GameObject;
            }
            
            
            this.InvokeRepeating("Prawn", 1f, 1f);
        }

        GUILayout.TextArea("Curr:" + list.Count.ToString() + ", total: "+totalCount);
    }

    public void Update()
    {
        if(list.Count > ConstantVar.currCount)
        {
            PoolManager.Pools[PoolName].Despawn(list[0].transform);
            list.RemoveAt(0);
        }
    }

}
