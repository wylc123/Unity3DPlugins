package com.xifarm.unity3dPlugins;

import com.unity3d.player.UnityPlayerActivity;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends UnityPlayerActivity  {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
		
		Log.d("MainActivity", "onCreate ok");
	}
	
	//测试功能
	public int Max(int a, int b)
	{
		return Math.max(a, b);
	}
	
	//震动的时长，单位是毫秒
	 public void Vibrate(long milliseconds) { 
	        Vibrator vib = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE); 
	        vib.vibrate(milliseconds); 
	 }
	 
	 //获取设备名称，如Nokia 987
	 //http://www.cnblogs.com/mainroadlee/archive/2011/01/09/Get_Phone_Number_Model_SDKVersion_Information_in_Android_SDK.html
	 public String GetDeviceMole()
	 {
		 return String.format("DEVICE:%s, MODEL:%s,MANUFACTURER:%s", 
				 Build.DEVICE, Build.MODEL, Build.MANUFACTURER);
	 }
	 
	 //Unity3D研究院之打开Activity与调用JAVA代码传递参数（十八）
	 //http://www.xuanyusong.com/archives/667
	 public void StartActivity1(String displayText)
	 {
		 Intent intent = new Intent(this, Activity1.class);
		 intent.putExtra("name", displayText);
		 this.startActivity(intent);
	 }
	 
	 public void StartActivity2(String displayText)
	 {
		 Intent intent = new Intent(this, Activity2.class);
		 intent.putExtra("name", displayText);
		 this.startActivity(intent);
	 }
	 
	 //http://www.cnblogs.com/ycxyyzw/archive/2013/03/12/2955845.html
	 //Can't create handler inside thread that has not called Looper.prepare()
	 //http://blog.csdn.net/woty123/article/details/6636368
	 public void ShowMessage2()
	 {
		 runOnUiThread(new Runnable() {
			public void run() {
				String info = "ShowMessage2!";
				Toast.makeText(MainActivity.this, info, Toast.LENGTH_SHORT).show();		
			}
		});
		 
	 }
	 	 
	 //final: C# -> JAVA failed. ==》 Success.  not final things.[P6800]
	 public void ShowMessage3(final String info)
	 {
		 runOnUiThread(new Runnable() {
			public void run() {				
				Toast.makeText(MainActivity.this, info, Toast.LENGTH_SHORT).show();		
			}
		});
	 }
	 
	 //Listener
	 private ConnectionChangeReceiver mConnectionChangeReceiver;
	 public void BeginAction(ExDataListener listener)
	 {
		 if(mConnectionChangeReceiver == null)
		 {
			 mConnectionChangeReceiver = new ConnectionChangeReceiver();
			 mConnectionChangeReceiver.SetListener(listener);
			 
			 IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		     this.registerReceiver(mConnectionChangeReceiver, filter);
		 }
	 }
	 
	 public void EndAction()
	 {
		 if(mConnectionChangeReceiver != null)
		 {
			 this.unregisterReceiver(mConnectionChangeReceiver);
			 mConnectionChangeReceiver = null; 
		 }
	 } 
	 
	 //UnitySendMessage
	 public void BeginAction_UnitySendMessage()
	 {
		 if(mConnectionChangeReceiver == null)
		 {
			 mConnectionChangeReceiver = new ConnectionChangeReceiver();
			 mConnectionChangeReceiver.SetListener(new ExDataListenerClass());			 
			 IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		     this.registerReceiver(mConnectionChangeReceiver, filter);
		 }
	 }
	 
	 public void EndAction_UnitySendMessage()
	 {
		 EndAction();
	 }
}
