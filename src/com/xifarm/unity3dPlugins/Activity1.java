package com.xifarm.unity3dPlugins;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Activity1 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity1);
		
		TextView  text = (TextView)this.findViewById(R.id.textView1);
		text.setText(this.getIntent().getStringExtra("name"));
		
		Button button = (Button)this.findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener(){	
			@Override
			public void onClick(View v) {
				Activity1.this.finish();	
			}			
		});
	}
}
